#!/bin/bash

for test in 1 2 3 4 5 6 7
    do {
         echo PyFeyn example $test:
         for version in 0.9 0.12
             do {
                  echo \ PyX-$version:
                  PYTHONPATH=..:../pyx_versions/PyX-$version python2 ../examples/pyfeyn-test$test.py 2>&1 | grep rror
                }
         done
         for version in 0.13 0.15 0.16
             do {
                  echo \ PyX-$version:
                  PYTHONPATH=..:../pyx_versions/PyX-$version python3 ../examples/pyfeyn-test$test.py 2>&1 | grep rror
                }
         done
       }
    done

for test in 1 2
    do {
         echo FeynML test $test:
         for version in 0.9 0.12
             do {
                  echo \ PyX-$version:
                  PYTHONPATH=..:../pyx_versions/PyX-$version python2 ../examples/feynml-test$test.py 2>&1 | grep rror
                  PYTHONPATH=..:../pyx_versions/PyX-$version python2 ../mkfeyndiag ./test$test.xml 2>&1 | grep rror
                }
         done
         # PyX-0.13 has an obvious bug in pyx.color.tohexstring, so always fails on XML
         for version in 0.15 0.16
             do {
                  echo \ PyX-$version:
                  PYTHONPATH=..:../pyx_versions/PyX-$version python3 ../examples/feynml-test$test.py 2>&1 | grep rror
                  PYTHONPATH=..:../pyx_versions/PyX-$version python3 ../mkfeyndiag ./test$test.xml 2>&1 | grep rror
                }
         done
       }
    done
